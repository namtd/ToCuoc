﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToCuoc.Models;
using System.Web.Security;
namespace ToCuoc.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserModel us, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                /*if (UserModel.IsValid(us.user_name, us.password) != 0 && us.user_name.IndexOf(' ') == -1 && us.password.IndexOf(' ') == -1)
                {
                    UserModel userModel = new UserModel();
                    List<UserModel> user = userModel.getUserByName(us.user_name);
                    string donvi_id = "";
                    string user_name = "";
                    string donvi_ql = "";
                    foreach (UserModel u in user)
                    {
                        //Session["donvi_id"] = u.donvi_id;
                        donvi_id = u.donvi_id.ToString();
                        user_name = u.user_name.ToString();
                        donvi_ql = u.donvi_ql.ToString();

                    }
                    HttpCookie cookie = new HttpCookie("donvi_id", donvi_id);
                    cookie.Expires = DateTime.Now.AddMinutes(600);
                    Response.SetCookie(cookie);

                    HttpCookie cookie2 = new HttpCookie("user_name", user_name);
                    cookie2.Expires = DateTime.Now.AddMinutes(600);
                    Response.SetCookie(cookie2);

                    HttpCookie cookie3 = new HttpCookie("donvi_ql", donvi_ql);
                    cookie3.Expires = DateTime.Now.AddMinutes(600);
                    Response.SetCookie(cookie3);

                    FormsAuthentication.SetAuthCookie(us.user_name, us.RememberMe);
                    return RedirectToAction("BC_MLL_tonghop", "MLL");

                
                }
                else
                {
                    ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác");
                }*/
                return RedirectToAction("Index", "Home");
            }
            TempData["msg"] = "Tên đăng nhập hoặc mật khẩu không chính xác!";
            return RedirectToAction("Index");
            //return View(us);   
        }
        
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            //Session.Clear();
            Response.Cookies["donvi_id"].Expires = DateTime.Now.AddMinutes(-1);
            Response.Cookies["user_name"].Expires = DateTime.Now.AddMinutes(-1);
            Response.Cookies["donvi_ql"].Expires = DateTime.Now.AddMinutes(-1);
            return RedirectToAction("Index", "Home");
        }
    }
}
