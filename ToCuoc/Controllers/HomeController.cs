﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToCuoc.Models;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Data;

namespace ToCuoc.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        EntityModel EntityModel = new EntityModel();
        public string slthang = DateTime.Now.Year.ToString()+ DateTime.Now.Month.ToString("d2");
        public ActionResult Index()
        {
            ViewBag.slthang = slthang;
            ViewBag.excel_data= TempData["excel_data"];
            return View();
        }
        [HttpPost]
        public ActionResult Index(UserModel u)
        {
            ViewBag.excel_data = Request.Form["excel_data"];
            string excel_data = Request.Form["excel_data"].Substring(0,Request.Form["excel_data"].Length-2);
            //string account = excel_data.Replace("\r\n",",");
            string account_str = "";
            string[] account_array = Regex.Split(excel_data,"\r\n");

            string sql = "";
            for(int i=0;i<account_array.Length; i++)
            {
                sql += "SELECT '"+account_array[i].Trim()+"' account FROM DUAL UNION ALL ";
                account_str += "'"+ account_array[i].Trim() + "',";
            }
            sql = sql.Substring(0,sql.Length-9);
            sql += " MINUS ";
            
            sql += "SELECT a.account FROM adsl.th_sdt_201706 a, cuocth_ftth_ds b where a.account in(";
            sql += account_str.Substring(0,account_str.Length-1); 
            sql += ") and a.account=b.calling group by a.account";

            List<EntityModel> obj = EntityModel.check_account(sql);
            string account_sai = "<table>";
            for (int i = 0; i < obj.ToArray().Length; i++)
            {
                //account_sai += obj[i].account + "\r\n";
                account_sai += "<tr><td>"+ obj[i].account + "</td></tr>";
            }
            account_sai += "</table>";
            ViewBag.account_sai = account_sai;
            return View();

        }
        [HttpPost]
        public ActionResult submit(UserModel u)
        {

            TempData["excel_data"] = Request.Form["hidden_data"];
            return RedirectToAction("Index");
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            //Session.Clear();
            Response.Cookies["donvi_id"].Expires = DateTime.Now.AddMinutes(-1);
            Response.Cookies["user_name"].Expires = DateTime.Now.AddMinutes(-1);
            Response.Cookies["donvi_ql"].Expires = DateTime.Now.AddMinutes(-1);
            return RedirectToAction("Index", "Home");
        }
    }
}
