﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.DataAccess.Client;
using System.Configuration;
//using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations;
using VNP_DoiSoatSoLieu.Models;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using System.Data;
//using System.Data.OracleClient;
namespace ToCuoc.Models
{
    public class UserModel
    {
        [Required(ErrorMessage = "Tên đăng nhập không được để trống!")]
        public string user_name { get; set; }
        [Required(ErrorMessage = "Mật khẩu không được để trống!")]
        public string password { get; set; }
        public int donvi_id { get; set; }
        public SelectList donvi { get; set; }
        public string status { get; set; }
        public string ten { get; set; }
        public string donvi_ql { get; set; }
        public bool RememberMe { get; set; }

        public List<UserModel> getUser()
        {
            List<UserModel> user_ = new List<UserModel>();
            
            var odr = BaseClass.Lib_GetDataReader("select a.*,b.ten  from nguoi_dung a,quan_huyen b where a.quan_huyen_id=b.quan_huyen_id");
            foreach (DataRow item in odr.Rows)
            {
                user_.Add(new UserModel
                {
                    user_name = item["user_name"].ToString(),
                    password = item["password"].ToString(),
                    donvi_id = Convert.ToInt32(item["donvi_id"].ToString()),
                    status = item["status"].ToString(),
                    ten = item["ten"].ToString()
                });
            }
            return user_;
        }
        public List<UserModel> getUserByName(string name)
        {
            List<UserModel> user_ = new List<UserModel>();
            var odr = BaseClass.Lib_GetDataReader("select * from users where user_name='" + name + "'");
            foreach (DataRow item in odr.Rows)
            {
                user_.Add(new UserModel
                {
                    user_name = item["user_name"].ToString(),
                    password = item["password"].ToString(),
                    donvi_id = Convert.ToInt32(item["donvi_id"].ToString()),
                    status = item["status"].ToString(),
                    donvi_ql= item["donvi_ql"].ToString(),
                    ten = item["ten"].ToString()
                });
            }
            return user_;
        }
        public static int IsValid(string username, string password)
        {
            string sql = "SELECT * FROM users WHERE user_name='" + username + "' and password='" + password + "' and status=1";
            //var cc = BaseClass.ConnectDataBase();
            return BaseClass.Lib_CheckExist(sql);
        }

        public void deleteUser(int id)
        {
            string sql = "DELETE FROM nguoi_dung WHERE id=" + id + "";
            BaseClass.Lib_ExecuteNonQuery(sql);
        }
        public int checkExist(UserModel us)
        {
            string sql = "SELECT * FROM nguoi_dung WHERE user_name='" + us.user_name + "'";
            return BaseClass.Lib_CheckExist(sql);
        }
        public List<UserModel> getDonvi()
        {
            List<UserModel> user_ = new List<UserModel>();
            var odr = BaseClass.Lib_GetDataReader("select quan_huyen_id,ten from quan_huyen");
            foreach (DataRow item in odr.Rows)
            {
                user_.Add(new UserModel
                {
                    donvi_id = Convert.ToInt32(item["donvi_id"].ToString()),
                    ten = item["ten"].ToString()
                });
            }
            return user_;
        }

    }
    public class GiamTruINS
    {
        public string goi_cuoc { get; set; }
        public string giam_tru { get; set; }
        public List<GiamTruINS> GiamTru_INS()
        {
            List<GiamTruINS> obj = new List<GiamTruINS>();
            obj.Add(new GiamTruINS { goi_cuoc = "F200T200P0_B0", giam_tru = "155000" });
            obj.Add(new GiamTruINS { goi_cuoc = "F160T160P0_B0", giam_tru = "75000" });
            obj.Add(new GiamTruINS{goi_cuoc = "F120T120P0_B1",giam_tru= "46818" });
            obj.Add(new GiamTruINS { goi_cuoc = "F80T80P0_B2", giam_tru = "35000" });
            return obj;
        }
    }
    public class UserAssociateModel
    {
        public int id { get; set; }
        public string user_name { get; set; }
        public string password { get; set; }
        public string stations_name { get; set; }
        public string main_stations_name { get; set; }
        public string quyen { get; set; }
        public string status { get; set; }
        public string ho_ten { get; set; }
        public string so_dt { get; set; }
        public string email { get; set; }
        public string ghi_chu { get; set; }
        public bool RememberMe { get; set; }
        /*public List<UserAssociateModel> getUser()
        {
            List<UserAssociateModel> user_ = new List<UserAssociateModel>();
            OracleDataReader odr = BaseClass.Lib_GetDataReader("SELECT a.id,a.user_name,a.password,b.name,c.name,d.name,a.status,a.ho_ten,a.so_dt,a.email,a.ghi_chu FROM nguoi_dung a,stations b,main_stations c,permission d where a.stations_id=b.id and a.main_stations_id=c.id and a.quyen=d.id order by a.id");
            while (odr.Read())
                user_.Add(new UserAssociateModel { id = Convert.ToInt32(odr[0].ToString()), user_name = odr[1].ToString(), password = odr[2].ToString(), stations_name = odr[3].ToString(), main_stations_name = odr[4].ToString(), quyen = odr[5].ToString(), status = Convert.ToInt32(odr[6].ToString()) == 1 ? "Bình thường" : "Khóa", ho_ten = odr[7].ToString(), so_dt = odr[8].ToString(), email = odr[9].ToString(), ghi_chu = odr[10].ToString() });
            return user_;
        }
        public List<UserAssociateModel> getUserByName(string name)
        {
            List<UserAssociateModel> user_ = new List<UserAssociateModel>();
            OracleDataReader odr = BaseClass.Lib_GetDataReader("SELECT a.id,a.user_name,a.password,b.name,c.name,d.name,a.status,a.ho_ten,a.so_dt,a.email,a.ghi_chu FROM nguoi_dung a,stations b,main_stations c,permission d where a.stations_id=b.id and a.main_stations_id=c.id and a.quyen=d.id and a.user_name='" + name + "' order by a.id");
            while (odr.Read())
                user_.Add(new UserAssociateModel { id = Convert.ToInt32(odr[0].ToString()), user_name = odr[1].ToString(), password = odr[2].ToString(), stations_name = odr[3].ToString(), main_stations_name = odr[4].ToString(), quyen = odr[5].ToString(), status = Convert.ToInt32(odr[6].ToString()) == 1 ? "Bình thường" : "Khóa", ho_ten = odr[7].ToString(), so_dt = odr[8].ToString(), email = odr[9].ToString(), ghi_chu = odr[10].ToString() });
            return user_;
        }*/
    }

}
