﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.DataAccess.Client;
using System.Configuration;
//using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations;
using VNP_DoiSoatSoLieu.Models;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using System.Data;
//using System.Data.OracleClient;
namespace ToCuoc.Models
{
    public class EntityModel
    {
        
        public string account { get; set; }

        public List<EntityModel> check_account(string sql)
        {
            List<EntityModel> obj = new List<EntityModel>();
            
            var odr = BaseClass.Lib_GetDataReader(sql);
            foreach (DataRow item in odr.Rows)
            {
                obj.Add(new EntityModel
                {
                    account = item["account"].ToString()
                });
            }
            return obj;
        }
        
    }

}
