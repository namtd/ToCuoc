﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.DataAccess.Client;
using System.Data;
using System.Configuration;
//using System.Data.OracleClient;
using System.IO;
//using Excel = Microsoft.Office.Interop.Excel;
namespace VNP_DoiSoatSoLieu.Models
{
    public class BaseClass
    {
        public static OracleConnection ConnectDataBase()
        {
            OracleConnection conn = new OracleConnection(ConfigurationManager.ConnectionStrings["bcrm"].ConnectionString);
            return conn;
        }
        public static OracleConnection conn_global;

        public static DataSet Lib_GetDataSet(string sql)
        {
            conn_global = BaseClass.ConnectDataBase();
            OracleCommand omd = new OracleCommand(sql, conn_global);
            OracleDataAdapter oda = new OracleDataAdapter(omd);
            DataSet ds = new DataSet();
            try
            {
                conn_global.Open();
            }
            catch
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Lỗi kết nối với cơ sở dữ liệu!')</SCRIPT>");
            }
            oda.Fill(ds, "0");
            return ds;
        }
        public static void Lib_ExecuteNonQuery(string sql)
        {
            conn_global = BaseClass.ConnectDataBase();
            OracleCommand omd = new OracleCommand(sql, conn_global);
            try
            {
                conn_global.Open();

            }
            catch
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Lỗi kết nối với cơ sở dữ liệu!')</SCRIPT>");
            }
            omd.ExecuteNonQuery();
            conn_global.Close();
        }

        public static DataTable Lib_GetDataReader(string sql)
        {
            try
            {
                using (var cnn = ConnectDataBase())
                {
                    DataTable dt = new DataTable();
                    OracleCommand cmd = new OracleCommand(sql, cnn);
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    da.Fill(dt);
                    return dt;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }
        public static int Lib_CheckExist(string sql)
        {
            conn_global = BaseClass.ConnectDataBase();
            OracleCommand omd = new OracleCommand(sql, conn_global);
            try
            {
                conn_global.Open();
            }
            catch
            {
                System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Lỗi kết nối với cơ sở dữ liệu!')</SCRIPT>");
            }
            OracleDataReader odr = omd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(odr);
            conn_global.Close();
            return dt.Rows.Count;
        }



        
        static public void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                throw new Exception("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }


    }
}
